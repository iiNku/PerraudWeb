# Perraud
Site web + app

# TODO LÉO

Formulaire d'inscription des entreprise, 
formulaire d'inscription des élèves,
envois des mails pour le mot de passe, 
faire la page de rendez vous pour les entreprise
faire la page de classement pour les élève, 
faire les pages pour voir les informations d'un eleve, 
les informations d'une entreprise, 
faire la page d'entretient en cours/entretient suivant  
uploader un CV pour les élèves  
mettre en place la gestion du planning pour perraud  
imprimer un planning pour une entreprise  
imprimier un planning pour un eleve  
imprimer un planning pour tous  
revoir la gestion des salles  
faire tous le systeme de notification en temps réel  
faire l'api  
faire l'application mobile 
tout installer sur le serveur
faire les tsets de monté en chare


# Setup vagrant box

- Install lastest VirtualBox version : https://www.virtualbox.org/wiki/Downloads
- Install lastest Vagrant version : http://www.vagrantup.com/downloads.html
- Add ```192.168.56.101``` to your host
    - For windows user : 
        - C:\Windows\System32\drivers\etc\hosts
        - Add ``` 192.168.56.101  local.dev ```
    - For OSX user :
        - ```sudo nano /private/etc/hosts  ```
        - Add ``` 192.168.56.101  local.dev ```
- In a terminal, run ```vagrant plugin install vagrant-winnfsd```
- Go on ```/vagrant``` and run ```vagrant up```
- When your VM is up, run ```vagrant ssh```
- On the vm, run ```sudo apt-get install php5-curl```
- Install symfony's dependency : 
    - ```cd /var/www```
    - ```composer install```

# Connect to Database

- Mysql is installed with the box

## For windows user :
 - Download Heidsql : http://www.heidisql.com/
 - Add a new session : 
    - Type de réseau : MySQL (SSH tunnel)
    - Nom ou IP de l'hôte : 127.0.0.1
    - Utilisateur : root
    - Mot de passe : root
    - Port : 3306
- On Tunnel SSH tab : 
    - Download plink.exe and enter its path
    - Hôte et port SSH : local.dev      22
    - Nom d'utilisateur : vagrant
    - Leave blank the field "Mot de passe"
    - Fichier de clé privée : ```/vagrant/puphpet/files/dot/ssh/id_rsa.ppk```
    - Port local : 3306
- If the connection isn't working, open a terminal and type : ```"C:\Program Files (x86)\PuTTY\plink.exe" local.dev``` and type Y to confirm "Store the private key"
