var app = require('http').createServer(handler)
var io = require('socket.io')(app);
var fs = require('fs');

app.listen(8080);

function handler (req, res) {
  console.log("handler called");
}

io.on('connection', function (socket) {
	socket.on('notification', function (from, to) {
		console.log("Notification from " + from + " to " + to);
		console.log(to + ' : ' + from + ' sera en retard de 5 minutes.');
		socket.broadcast.emit('late', to, from + ' sera en retard de 5 minutes.');
  	});
});