<?php

namespace ApiRestBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UserBundle\Entity\User;
use UserBundle\Form\CompanyEditType;
use UserBundle\Form\StudentEditType;


class APIController extends FOSRestController
{
    /**
     * @Rest\View()
     * @param $id
     * @return array
     */
    public function getCompanyProfileAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->findOneById($id);
        $repo = $em->getRepository('UserBundle:Company');
        $company = $repo->findOneBySupervisor($user);
        if(empty($company))
            throw new NotFoundHttpException("L'entreprise n'existe pas.");
        return ['company' => $company];
    }

    /**
     * @Rest\View()
     * @return array
     */
    public function getCompaniesAllAction()
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('UserBundle:Company');
        $companies = $repo->findAll();
        return ['companies' => $companies];
    }

    /**
     * @Rest\View()
     * @param $id
     * @return array
     */
    public function getCompanyFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->findOneById($id);
        $repo = $em->getRepository('UserBundle:Company');
        $company = $repo->findOneBySupervisor($user);
        if(empty($company))
            throw new NotFoundHttpException("L'entreprise n'existe pas.");

        $form = $this->createForm(new CompanyEditType(), $company);
        return ['form' => $form->createView()];
    }

    /**
     * @Rest\View()
     * @param Request $request
     * @param $id
     * @return array
     */
    public function postCompanyEditAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->findOneById($id);
        $repo = $em->getRepository('UserBundle:Company');
        $company = $repo->findOneBySupervisor($user);
        if(empty($company))
            throw new NotFoundHttpException("L'entreprise n'existe pas.");

        $form = $this->createForm(new CompanyEditType(), $company);
        $form->handleRequest($request);
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return ['succes' => true];
        }
        return ['succes' => false];
    }

    /**
     * @Rest\View()
     * @param $id
     * @return array
     */
    public function getStudentProfileAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->findOneById($id);
        $repo = $em->getRepository('UserBundle:Student');
        $student = $repo->findOneByUser($user);
        if(empty($student))
            throw new NotFoundHttpException("L'étudiant n'existe pas.");
        return ['student' => $student];
    }

    /**
     * @Rest\View()
     * @return array
     */
    public function getStudentsAllAction() {
        $students = $this->getDoctrine()->getManager()->getRepository('UserBundle:Student')->findAll();
        return ['students' => $students];
    }

    /**
     * @Rest\View()
     * @param $id
     * @return array
     */
    public function getStudentFormAction($id){

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->findOneById($id);
        $repo = $em->getRepository('UserBundle:Student');
        $student = $repo->findOneByUser($user);
        if(empty($student))
            throw new NotFoundHttpException("L'étudiant n'existe pas.");

        $form = $this->createForm(new StudentEditType(), $student);
        return ['form' => $form->createView()];
    }

    /**
     * @Rest\View()
     * @param Request $request
     * @param $id
     * @return array
     */
    public function postStudentEditAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->findOneById($id);
        $repo = $em->getRepository('UserBundle:Student');
        $student = $repo->findOneByUser($user);
        if(empty($student))
            throw new NotFoundHttpException("L'étudiant n'existe pas.");

        $form = $this->createForm(new StudentEditType(), $student);
        $form->handleRequest($request);
        if($form->isValid()){
            $em->flush();
            return ['succes' => true];
        }
        return ['succes' => false];
    }

    /**
     * @Rest\View()
     * @param $id
     * @return array
     */
    public function getMeetingAllAction($id) {
        $em = $this->getDoctrine()->getManager();
        $meetings = null;
        $user = $em->getRepository('UserBundle:User')->findOneById($id);
        $repo = $em->getRepository('MeetingBundle:Meeting');
        if($user->hasRole("ROLE_STUDENT")) {
            $repoStudent = $em->getRepository('UserBundle:Student');
            $student = $repoStudent->findOneByUser($user);
            $meetings = $repo->findByStudent($student);
        } else if($user->hasRole("ROLE_COMPANY")) {
            $repoCompany = $em->getRepository('UserBundle:Company');
            $company = $repoCompany->findOneBySupervisor($user);
            $meetings = $repo->findByCompany($company);
        }
        return ['meetings' => $meetings];
    }

    /**
     * @Rest\View()
     * @param $id
     * @return array
     */
    public function getMeetingAction($id) {
        $repo = $this->getDoctrine()->getManager()->getRepository('MeetingBundle:Meeting');
        $meeting = $repo->findOneById($id);
        return ['meeting' => $meeting];
    }

}
