<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexdAction()
    {
        if($this->getUser())
            return $this->redirect($this->generateUrl('dashboard'));
        else
            return $this->render('home/home.html.twig');
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboardAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('planning_show'));
        }
        else if ($this->get('security.authorization_checker')->isGranted('ROLE_COMPANY')) {
            return $this->redirect($this->generateUrl('company_planning'));
        }
        else{
            return $this->redirect($this->generateUrl('student_planning'));
        }
    }
}
