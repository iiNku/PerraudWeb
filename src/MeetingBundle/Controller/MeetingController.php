<?php

namespace MeetingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use MeetingBundle\Entity\Meeting;
use MeetingBundle\Form\MeetingType;
use Symfony\Component\Routing\Annotation\Route;

class MeetingController extends Controller
{
    /**
     * @Route("/", name="meeting_homepage")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $params = array();
        $meetingRepo = $this->getDoctrine()->getManager()->getRepository("MeetingBundle:Meeting");
        $meetings = $meetingRepo->findAll();
        $params["meetings"] = $meetings;

        return $this->render('MeetingBundle:Meetings:index.html.twig', $params);
    }


    /**
     * @Route("/add", name="meeting_add")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $meeting = new Meeting();

        $form = $this->getForm($meeting);

        if ($form->handleRequest($request)->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $temp = $em->getRepository("MeetingBundle:Meeting")->findby(["student" => $meeting->getStudent(),
                                                                        "company" => $meeting->getCompany()]);
            if($temp == null) {
                $em->persist($meeting);
                $em->flush();
                // On redirige vers la page de visualisation de l'annonce nouvellement créée
                $request->getSession()->getFlashBag()->add("message", "Le meeting a été ajouté.");
            } else {
                $request->getSession()->getFlashBag()->add("message", "Le meeting existe déjà.");
            }

            return $this->redirect($this->generateUrl('meeting_add'));
        }

        return $this->render('MeetingBundle:Meetings:add.html.twig', array("form" => $form->createView()));
    }

    public function showAction($id)
    {
        $meeting = $this->getDoctrine()->getManager()->getRepository("MeetingBundle:Meeting")->find($id);
        return $this->render('MeetingBundle:Meetings:show.html.twig', array("meeting" => $meeting));
    }

    public function editAction($id, Request $request)
    {
        $meeting = $this->getDoctrine()->getManager()->getRepository("MeetingBundle:Meeting")->find($id);

        $form = $this->getForm($meeting);

        if ($form->handleRequest($request)->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($meeting);
            $em->flush();

            // On redirige vers la page de visualisation de l'annonce nouvellement créée
            return $this->redirect($this->generateUrl('meeting_homepage'));
        }
        return $this->render('MeetingBundle:Meetings:edit.html.twig', array("form" => $form->createView()));
    }

    public function deleteAction($id)
    {
        $meeting = $this->getDoctrine()->getManager()->getRepository("MeetingBundle:Meeting")->find($id);

        if($meeting != null )
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($meeting);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('meeting_homepage'));
    }

    /**
     * @Route("/generate", name="meeting_generate")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function generatePlanningAction()
    {
        $service = $this->get("meeting.plannification");
        $service->plan();
        exit;
        return $this->redirect($this->generateUrl('meeting_homepage'));
    }

    private function getForm($meeting)
    {
        return $this->get('form.factory')->create(new MeetingType(), $meeting);
    }

}
