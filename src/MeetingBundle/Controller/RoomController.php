<?php

namespace MeetingBundle\Controller;

use MeetingBundle\Form\RoomType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MeetingBundle\Entity\Room;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class RoomController extends Controller
{
    /**
     * @Route("/", name="room_list")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('MeetingBundle:Room');

        $rooms = $repo->findAll();

        return $this->render('MeetingBundle:Rooms:index.html.twig', ['rooms' => $rooms]);
    }

    /**
     * @Route("/add", name="room_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $room = new Room();

        $form = $this->createForm(new RoomType(), $room);
        $form->handleRequest($request);

        if($form->isValid()){
            $em->persist($room);
            $em->flush();

            $request->getSession()->getFlashBag()->add("message", "La salle a été ajoutée.");
            return $this->redirect($this->generateUrl('room_list'));
        }

        return $this->render('MeetingBundle:Rooms:add.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete/{id}", name="room_delete")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Room $room
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Request $request, Room $room){

        $em = $this->getDoctrine()->getManager();

        $companies = $em->getRepository('UserBundle:Company')->findByRoom($room);

        foreach($companies as $company) {
            $company->setRoom(null);
            $em->persist($company);
        }

        $em->remove($room);
        $em->flush();

        $request->getSession()->getFlashBag()->add("message", "La salle a été supprimée.");
        return $this->redirect($this->generateUrl('room_list'));
    }
}