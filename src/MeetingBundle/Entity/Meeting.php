<?php

namespace MeetingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Meeting
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MeetingBundle\Entity\MeetingRepository")
 */
class Meeting
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startHour", type="datetime", nullable=true)
     */
    private $startHour;

    /**
     * @var integer
     * @Assert\Blank()
     * @ORM\Column(name="duration", type="integer", nullable=true, options={"default":30})
     */
    private $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=true)
     * @Assert\NotBlank()
     */
    private $priority;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Student", cascade={"persist"})
     * @Assert\NotBlank()
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Company", cascade={"persist"})
     * @Assert\NotBlank()
     */
    private $company;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startHour
     *
     * @param \DateTime $startHour
     * @return Meeting
     */
    public function setStartHour($startHour)
    {
        $this->startHour = $startHour;

        return $this;
    }

    /**
     * Get startHour
     *
     * @return \DateTime 
     */
    public function getStartHour()
    {
        return $this->startHour;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Meeting
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return Meeting
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set student
     *
     * @param \UserBundle\Entity\Student $student
     * @return Meeting
     */
    public function setStudent(\UserBundle\Entity\Student $student = null)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return \UserBundle\Entity\Student 
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set company
     *
     * @param \UserBundle\Entity\Company $company
     * @return Meeting
     */
    public function setCompany(\UserBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \UserBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
}
