<?php

namespace MeetingBundle\Entity;

use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\Student;
use UserBundle\Entity\Company;

/**
 * MeetingRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MeetingRepository extends EntityRepository
{
    public function findByNoHour()
    {
        $query = $this->createQueryBuilder('m')->addSelect('m')->where('m.startHour IS NULL');
        return $query->getQuery()->getResult();
    }

    public function findConflits($student, $startHour, $company){
        $query = $this->createQueryBuilder('m')
            ->addSelect('m')
            ->where('m.student = :student')
            ->andWhere('m.startHour = :startHour')
            ->andWhere('m.company != :company')
            ->setParameters(['student' => $student, 'company' => $company, 'startHour' => $startHour]);

        return $query->getQuery()->getResult();
    }
    public function findByPriorityCompany($company){

        $query = $this->createQueryBuilder('m')
            ->addSelect('m')
            ->where('m.company = :company')
            ->andWhere("m.startHour IS NULL")
            ->setParameters(array('company' => $company))
            ->setMaxResults(1);
        return $query->getQuery()->getOneOrNullResult();
    }

    public function findByPriorityCompanyArray($company){
        $query = $this->createQueryBuilder('m')
            ->addSelect('m')
            ->where('m.company = :company')
            ->andWhere("m.startHour IS NULL")
            ->setParameters(array('company' => $company))
            ->orderBy('m.priority', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function CompaniesByMeetings(){

        $query = $this->createQueryBuilder('m')
            ->addSelect('m', 'count(m.company) AS HIDDEN nbmeeting')
            ->addGroupBy('m.company')
            ->orderBy('nbmeeting','ASC');

        return $query->getQuery()->getResult();
    }

    public function countMeetings(Student $student){

        $query = $this->createQueryBuilder('m')
            ->addSelect('count(m) as nb')
            ->where('m.student = :student')
            ->setParameter('student', $student);

        return $query->getQuery()->getSingleResult();
    }

    public function countMeetingsCompany(Company $company){

        $query = $this->createQueryBuilder('m')
            ->addSelect('count(m) as nb')
            ->where('m.company = :company')
            ->setParameter('company', $company);

        return $query->getQuery()->getSingleResult();
    }

    public function findNextMeeting(Company $company, $hour){

        $query = $this->createQueryBuilder('m')
            ->addSelect('m')
            ->where('m.company = :company')
            ->andWhere('m.startHour > :hour')
            ->orderBy('m.startHour', 'ASC')
            ->setParameters(['company' => $company, 'hour' => $hour])
            ->setMaxResults(1);

        return $query->getQuery()->getOneOrNullResult();
    }
}
