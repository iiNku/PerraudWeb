<?php
/**
 * Created by PhpStorm.
 * User: Nathou
 * Date: 03/09/2015
 * Time: 14:57
 */
namespace MeetingBundle\Service;
use Doctrine\ORM\EntityManager;
use DateTime;

class MeetingService {

    private $em;

    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    /**
     *
     */
    public function plan(){
        $repoCompanies = $this->em->getRepository('UserBundle:Company');
        $repoMeeting = $this->em->getRepository('MeetingBundle:Meeting');
        $repoRooms = $this->em->getRepository('MeetingBundle:Room');
        $repoStudents = $this->em->getRepository('UserBundle:Student');

        $planningStartHour = $this->em->getRepository('UserBundle:Configuration')->find(1)->getBegin();

        $meetings = $repoMeeting->findAll();

        $rooms = $repoRooms->findAll();

        foreach($meetings as $meeting)
        {
            $meeting->setStartHour(null);
            $this->em->persist($meeting);
        }
        $this->em->flush();

        $meetingscompanies = $repoMeeting->CompaniesByMeetings();
        $companies = array();

        foreach($meetingscompanies as $meetingcompany)
        {
            $companies[] = $meetingcompany->getCompany();
        }

        //Attribution des salles pour les entreprises
        $i = 0;
        $nbCompany = sizeof($companies);
        foreach($rooms as $room) {
            $capacity = $room->getCapacity();
            $indice = $i + $capacity;
            if($indice > $nbCompany) {
                $indice = $nbCompany;
            }
            while($i < $indice) {
                $company = $companies[$i];
                $company->setRoom($room);
                $i++;
            }
        }

        //$companies = $repoMeeting->CompaniesByMeetings();
        $meetings = $repoMeeting->findByNoHour();

        $hour = new DateTime();
        $hourPlanning = split(':', $planningStartHour);
        $hour->setTime($hourPlanning[0],$hourPlanning[1],0);
        $dateRdv = $hour;
        while(!empty($meetings))
        {
            $studentsMeeting = $repoStudents->findAll();
            foreach($companies as $company)
            {
                $meetingsCompany = $repoMeeting->findByPriorityCompanyArray($company);
                foreach($meetingsCompany as $meeting)
                {
                    $student = $meeting->getStudent();
                    if(in_array($student, $studentsMeeting))
                    {
                        $meeting->setStartHour($dateRdv);
                        $meeting->setDuration(30);
                        $this->em->persist($company);
                        $this->em->persist($student);
                        $this->em->persist($meeting);
                        $this->em->flush();
                        $meetings = self::removeMeeting($meetings, $meeting);
                        $studentsMeeting = self::removeStudent($studentsMeeting, $student);
                        break;
                    }
                }

            }
            $dateRdv->add(\DateInterval::createFromDateString("+30 minutes"));
            //usort($companies, "self::cmp");
        }
        print_r("OK");
    }

    function cmp($a, $b)
    {
        if ($a->getPriority() == $b->getPriority()) {
            return 0;
        }
        return ($a->getPriority() < $b->getPriority()) ? 1 : -1;
    }

    function removeMeeting($meetings, $meeting)
    {
        foreach($meetings as $key=>$m)
        {
            if($m->getId() == $meeting->getId())
            {
                unset($meetings[$key]);
            }
        }
        return $meetings;
    }

    function removeStudent($students, $student)
    {
        foreach($students as $key=>$s)
        {
            if($s->getId() == $student->getId())
            {
                unset($students[$key]);
            }
        }
        return $students;
    }
}