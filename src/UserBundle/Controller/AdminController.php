<?php
/**
 * Created by PhpStorm.
 * User: iinku
 * Date: 20/10/15
 * Time: 15:58
 */

namespace UserBundle\Controller;


use MeetingBundle\Entity\Meeting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use UserBundle\Entity\Company;
use UserBundle\Form\AdminEditType;
use UserBundle\Entity\User;
use UserBundle\Form\UserType;
use UserBundle\Entity\Student;
use UserBundle\Form\CompanyType;
use UserBundle\Entity\Message;
use UserBundle\Entity\Configuration;


class AdminController extends Controller
{
    /**
     * @Route("/message", name="admin_message_notification")
     * @Security("has_role('ROLE_ADMIN')")
     * @param int $limit
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function messageAction($limit = 5){

        $em = $this->getDoctrine()->getManager();
        $repoMessage = $em->getRepository('UserBundle:Message');
        $messages = $repoMessage->findBy(
            ['unread' => 1],
            ['created' => 'DESC'],
            5
        );
        return $this->render('UserBundle:Admin:message_notification.html.twig', ['messages' => $messages]);
    }

    /**
     * @Route("/message/list", name="admin_message_list")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function messageListAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $repoMessage = $em->getRepository('UserBundle:Message');
        $messages = $repoMessage->findAll();

        return $this->render('UserBundle:Admin:message_list.html.twig', ['messages' => $messages]);
    }

    /**
     * @Route("/message/read/{id}", name="admin_message_read")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Message $message
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function messageReadAction(Request $request, Message $message){
        $message->setUnread(false);

        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        $request->getSession()->getFlashBag()->add("message", "Le message a été marqué comme lu.");
        return $this->redirect($this->generateUrl('admin_message_list'));
    }

    /**
     * @Route("/message/delete/{id}", name="admin_message_delete")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Message $message
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function messageDeleteAction(Request $request, Message $message){

        $em = $this->getDoctrine()->getManager();
        $em->remove($message);
        $em->flush();
        $request->getSession()->getFlashBag()->add("message", "Le message a été supprimé.");
        return $this->redirect($this->generateUrl('admin_message_list'));
    }

    /**
     * @Route("/message/show/{id}", name="admin_message_show")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Message $message
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function messageShowAction(Message $message){

        $message->setUnread(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        return $this->render('UserBundle:Admin:message_show.html.twig', ['message' => $message]);
    }

    /**
     * @Route("/student/register", name="admin_register_student")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return array
     */
    public function registerStudentAction(Request $request) {

        $user = new User();
        $formRegister = $this->createForm(new UserType(), $user);

        $formRegister->handleRequest($request);

        if($formRegister->isValid()){
            $nameStudent = $this->get('slugify')->slugify($user->getName() . "." . $user->getFirstname());

            $user->setUsername($nameStudent);
            $user->setPlainPassword($nameStudent);
            $user->addRole('ROLE_STUDENT');

            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());

            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);

            $student = new Student();
            $student->setUser($user);
            $student->setPriority(0);

            $em = $this->getDoctrine()->getManager();
            $em->persist($student);
            $em->flush();


            $message = \Swift_Message::newInstance()
                ->setSubject('Inscription sur forum-polytech.univ-lyon.fr')
                ->setFrom(["inscription@forum-polytech.univ-lyon1.fr" => "L'équipe Forum Polytech"])
                ->setTo($user->getEmail())
                ->setBody($this->renderView('UserBundle:Email:register.html.twig', ['user' => $user]))
            ;
            $message->setContentType("text/html");

            $this->get('mailer')->send($message);

            $request->getSession()->getFlashBag()->add("message", "L'étudiant a été ajouté.");
            return $this->redirect($this->generateUrl('admin_register_student'));
        }
        return $this->render('UserBundle:Admin:register_student.html.twig', ['form' => $formRegister->createView()]);
    }

    /**
     * @Route("/company/register", name="admin_register_company")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return array
     */
    public function registerAction(Request $request) {

        $user = new User();
        $company = new Company();
        $formRegister = $this->createForm(new UserType(), $user);
        $formCompany = $this->createForm(new CompanyType(),$company);

        $formRegister->handleRequest($request);
        $formCompany->handleRequest($request);

        if($formRegister->isValid() && $formCompany->isValid()){

            $nameStudent = $this->get('slugify')->slugify($user->getName() . "." . $user->getFirstname());
            $user->setUsername($nameStudent);
            $user->setPlainPassword($nameStudent);
            $user->addRole('ROLE_COMPANY');

            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());

            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);

            $company->setSupervisor($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('Inscription sur forum-polytech.univ-lyon.fr')
                ->setFrom(["inscription@forum-polytech.univ-lyon1.fr" => "L'équipe Forum Polytech"])
                ->setTo($user->getEmail())
                ->setBody($this->renderView('UserBundle:Email:register.html.twig', ['user' => $user]))
            ;
            $message->setContentType("text/html");

            $this->get('mailer')->send($message);

            $request->getSession()->getFlashBag()->add("message", "L'entreprise a été ajoutée");
            return $this->redirect($this->generateUrl('admin_register_company'));
        }

        return $this->render('UserBundle:Admin:register_company.html.twig', ['form' => $formRegister->createView(), 'formCompany' => $formCompany->createView()]);
    }

    /**
     * @Route("/student/list", name="admin_list_students")
     * @Security("has_role('ROLE_ADMIN')")
     * @return array
     */
    function listStudentAction() {
        $em = $this->getDoctrine()->getManager();
        $students = $em->getRepository("UserBundle:Student")->findAll();

        foreach($students as $student){
            $student->setNbEntretiens($em->getRepository('MeetingBundle:Meeting')->countMeetings($student)['nb']);
        }

        return $this->render('UserBundle:Admin:list_student.html.twig', ['students' => $students]);
    }

    /**
     * @Route("/list", name="admin_list_company")
     * @Security("has_role('ROLE_ADMIN')")
     * @return array
     */
    function listCompanyAction() {
        $em = $this->getDoctrine()->getManager();
        $companies = $em->getRepository("UserBundle:Company")->findAll();

        foreach($companies as $company){
            $company->setNbEntretiens($em->getRepository('MeetingBundle:Meeting')->countMeetingsCompany($company)['nb']);
        }

        return $this->render('UserBundle:Admin:list_company.html.twig', ['companies' => $companies]);
    }

    /**
     * @Route("/edit", name="admin_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @Template()
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request){

        $user = $this->getUser();
        $form = $this->createForm(new AdminEditType(), $user);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updateUser($user);

            $request->getSession()->getFlashBag()->add("message", "Votre profil a été mis à jour.");
            return $this->redirect($this->generateUrl('homepage'));
        }
        return ['form' => $form->createView(), 'user' => $user];
    }

    /**
     * @Route("/delete/company/{id}", name="delete_company")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteCompanyAction(Request $request, Company $company){

        $em = $this->getDoctrine()->getManager();
        $repoMeeting = $em->getRepository('MeetingBundle:Meeting');
        $meetings = $repoMeeting->findBy(['company' => $company]);
        foreach($meetings as $meeting){
            $em->remove($meeting);
        }
        $repoCommentary = $em->getRepository('UserBundle:Commentary');
        $commentaries = $repoCommentary->findBy(['company' => $company]);
        foreach($commentaries as $commentary){
            $em->remove($commentary);
        }
        $em->remove($company);



        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id'=>$company->getSupervisor()->getId()));

        $repoMessage = $em->getRepository('UserBundle:Message');
        $messages = $repoMessage->findBy(['user' => $user]);
        foreach($messages as $message){
            $em->remove($message);
        }

        $userManager->deleteUser($user);

        $em->flush();
        $request->getSession()->getFlashBag()->add('message', "L'entreprise a été supprimée.");
        return $this->redirect($this->generateUrl('admin_list_company'));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/planning", name="planning_show")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showPlanningAction(Request $request){

        $em = $this->getDoctrine()->getManager();

        $repoMeeting = $em->getRepository('MeetingBundle:Meeting');
        $meetings = $repoMeeting->findBy(
            [],
            ['startHour' => 'ASC']
        );

        $repoCompany = $em->getRepository('UserBundle:Company');
        $companies = $repoCompany->findAll();

        return $this->render('UserBundle:Admin:planning.html.twig', ['companies' => $companies, 'meetings' => $meetings]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/generate", name="planning_generate", options={"expose"=true})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function generatePlanningAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $rooms = $em->getRepository("MeetingBundle:Room")->findAll();
        $companies = $em->getRepository('UserBundle:Company')->findAll();

        $capacity = 0;
        foreach($rooms as $room) {
            $capacity += $room->getCapacity();
        }

        if(sizeof($companies) <= $capacity) {
            $service = $this->get("meeting.plannification");
            $service->plan();
        } else {
            $request->getSession()->getFlashBag()->add('message', "Il n'y a pas assez de places dans les salles pour accueillir toutes les entreprises.");
        }

        return $this->redirect($this->generateUrl('planning_show'));
    }

    /**
     * @Route("/change/{current}/{previous}", name="change_planning_previous", options={"expose"=true})
     * @Route("/change/{current}", name="change_planning", options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Meeting $current
     * @param Meeting $previous
     * @return JsonResponse
     * @internal param Meeting $meeting
     */
    public function changePlanningAction(Request $request, Meeting $current, Meeting $previous = null){

        $em = $this->getDoctrine()->getManager();
        $repoMeetings = $em->getRepository('MeetingBundle:Meeting');
        if($previous == null){
            $previous = $repoMeetings->findOneBy(['company' => $current->getCompany()], array('startHour' => 'ASC'));
        }
        $hourPrevious = clone $previous->getStartHour();
        $hourReference = clone $previous->getStartHour();
        $current->setStartHour(clone $hourReference);
        $hourReference->add(new \DateInterval('PT30M'));
        $otherMeeting = $repoMeetings->findConflits($current->getStudent(), $current->getStartHour(), $current->getCompany());
        if(sizeof($otherMeeting) > 0){
            $conflits = array();
            foreach($otherMeeting as $conflit){
                $conflits[] = $conflit->getId();
            }
            return new JsonResponse(['success' => false, 'message' => 'L\'étudiant a déjà un rendez-vous à cette heure-ci.', 'current' => $current->getId(), 'conflits' => $conflits]);
        }
        $meetings = $repoMeetings->findBy(['company' => $current->getCompany()], ['startHour' => 'ASC']);
        foreach($meetings as $meeting){
            if(($meeting->getStartHour() >= $hourPrevious || $meeting->getId() == $previous->getId()) && $meeting->getId() != $current->getId()){
                if($meeting->getStartHour() < $hourReference){
                    $meeting->setStartHour(clone $hourReference);
                    $otherMeeting = $repoMeetings->findBy(array('student' => $meeting->getStudent(), 'startHour' => $meeting->getStartHour()));
                    if(sizeof($otherMeeting) > 0){
                        $conflits = array();
                        foreach($otherMeeting as $conflit){
                            $conflits[] = $conflit->getId();
                            $conflits[] = $meeting->getId(); //wait me to discuss
                        }
                        return new JsonResponse(['success' => false, 'message' => 'L\'étudiant a déjà un rendez-vous à cette heure-ci.', 'current' => $current->getId(), 'conflits' => $conflits]);
                    }
                    $hourReference->add(new \DateInterval('PT30M'));
                }
            }
        }

        foreach($meetings as $meeting){
            $em->persist($meeting);
        }
        $em->flush();

        $hours = array();
        foreach($meetings as $meeting){
            $hours[$meeting->getId()] = $meeting->getStartHour()->format('h:i');
        }
        $data = array();
        $data['success'] = true;
        $data['message'] = 'Déplacement effectué.';
        $data['hours'] = $hours;

        return new JsonResponse($data);
    }

    /**
     * @Route("/delete/student/{student}", name="admin_delete_student", options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteStudentAction(Request $request, Student $student){

        $em = $this->getDoctrine()->getManager();
        $repoMeeting = $em->getRepository('MeetingBundle:Meeting');
        $meetings = $repoMeeting->findBy(['student' => $student]);
        foreach($meetings as $meeting){
            $em->remove($meeting);
        }
        $repoCommentary = $em->getRepository('UserBundle:Commentary');
        $commentaries = $repoCommentary->findBy(['student' => $student]);
        foreach($commentaries as $commentary){
            $em->remove($commentary);
        }
        $em->remove($student);

        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id'=>$student->getUser()->getId()));

        $repoMessage = $em->getRepository('UserBundle:Message');
        $messages = $repoMessage->findBy(['user' => $user]);
        foreach($messages as $message){
            $em->remove($message);
        }

        $userManager->deleteUser($user);

        $em->flush();
        $request->getSession()->getFlashBag()->add('message', "L'étudiant a été supprimé.");
        return $this->redirect($this->generateUrl('admin_list_students'));
    }

    /**
     * @Route("/activate/student/{student}", name="admin_activate_student")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function activateUserStudentAction(Request $request, Student $student){

        $user = $student->getUser();

        $userManager = $this->container->get('fos_user.user_manager');
        $user->setEnabled(true);
        $userManager->updateUser($user);
        $request->getSession()->getFlashBag()->add('message', "Le compte a été activé.");
        return $this->redirect($this->generateUrl('admin_list_students'));
    }

    /**
     * @Route("/activate/company/{company}", name="admin_activate_company")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function activateUserCompanyAction(Request $request, Company $company){

        $user = $company->getSupervisor();

        $userManager = $this->container->get('fos_user.user_manager');
        $user->setEnabled(true);
        $userManager->updateUser($user);
        $request->getSession()->getFlashBag()->add('message', "Le compte a été activé.");
        return $this->redirect($this->generateUrl('admin_list_company'));
    }

    /**
     * @Route("/configuration", name="admin_configuration")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function configurationAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $repoConfig = $em->getRepository('UserBundle:Configuration');
        $configuration = $repoConfig->find(1);
        if($configuration == null){
            $configuration = new Configuration();
            $configuration->setBegin('8:00');
        }

        if($request->request->get('begin') != null){
            $configuration->setBegin($request->request->get('begin'));
            $request->getSession()->getFlashBag()->add('message', "Le changement a été pris en compte.");
        }

        $em->persist($configuration);
        $em->flush();

        return $this->render('UserBundle:Admin:configuration.html.twig', ['begin' => $configuration->getBegin()]);
    }
}