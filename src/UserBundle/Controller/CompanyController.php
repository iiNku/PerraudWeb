<?php
/**
 * Created by IntelliJ IDEA.
 * User: Pierre-Mac
 * Date: 02/09/15
 * Time: 16:17
 */

namespace UserBundle\Controller;

use FOS\UserBundle\Form\Type\RegistrationFormType;
use FOS\UserBundle\Form\Type\UsernameFormType;
use MeetingBundle\Entity\Meeting;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UserBundle\Entity\Commentary;
use UserBundle\Entity\User;
use UserBundle\Form\CommentaryType;
use UserBundle\Form\CompanyEditType;
use UserBundle\Form\CompanyType;
use UserBundle\Entity\Company;
use UserBundle\Form\ImageType;
use UserBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use UserBundle\Entity\Student;

class CompanyController extends Controller {



    /**
     * @Route("/show/{id}", name="show_company")
     * @Security("has_role('ROLE_STUDENT') or has_role('ROLE_ADMIN')")
     * @Template()
     * @param Company $company
     * @return array
     */
    function showAction(Company $company) {
        return ['company' => $company];
    }

    /**
     * @Route("/edit", name="edit_company")
     * @Template()
     * @Security("has_role('ROLE_COMPANY')")
     * @param Company $company
     * @param Request $request
     * @return array
     */
    function editAction(Request $request) {

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $company = $em->getRepository('UserBundle:Company')->findOneBySupervisor($user);

        if(empty($company))
            throw new AccessDeniedException();

        $form = $this->createForm(new ImageType(), $user);
        $formCompany = $this->createForm(new CompanyEditType(),$company);

        $formCompany->handleRequest($request);
        $form->handleRequest($request);

        if($formCompany->isValid() && $form->isValid()){

            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updateUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $request->getSession()->getFlashBag()->add("message", "Votre profil a été mis à jour.");
            return $this->redirect($this->generateUrl('edit_company'));
        }

        return ['form' => $formCompany->createView(), 'formImage' => $form->createView()];
    }

    /**
     * @Route("/students/list", name="company_list_student")
     * @Security("has_role('ROLE_COMPANY')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function listStudentAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('UserBundle:Student');
        $students = $repo->findAll();

        $repoCompany = $em->getRepository('UserBundle:Company');
        $company = $repoCompany->findOneBySupervisor($this->getUser());

        if(empty($company)){
            throw new AccessDeniedException();
        }

        $repoMeeting = $em->getRepository('MeetingBundle:Meeting');
        $meetings = $repoMeeting->findBy(['company' => $company]);

        return $this->render('UserBundle:Company:students.html.twig', ['students' => $students, 'meetings' => $meetings]);
    }

    /**
     * @Route("/meetings", name="company_meeting_list")
     * @Security("has_role('ROLE_COMPANY')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function meetingsAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $repoCompany = $em->getRepository('UserBundle:Company');
        $company = $repoCompany->findOneBySupervisor($this->getUser());
        if(empty($company))
            throw new AccessDeniedException();

        $repoMeeting = $em->getRepository('MeetingBundle:Meeting');
        $meetings = $repoMeeting->findBy(['company' => $company]);

        return $this->render('UserBundle:Company:meetings.html.twig', ['meetings' => $meetings]);
    }

    /**
     * @Route("meeting/company/add/{student}/{priority}", name="company_add_meeting", options={"expose"=true})
     * @Security("has_role('ROLE_COMPANY')")
     * @param Request $request
     * @param Student $student
     * @param $priority
     * @return JsonResponse
     */
    public function addMeeting(Request $request, Student $student, $priority){

        $em = $this->getDoctrine()->getManager();
        $repoCompany = $em->getRepository('UserBundle:Company');
        $company = $repoCompany->findOneBySupervisor($this->getUser());

        if(empty($company)){
            throw new NotFoundHttpException();
        }

        if($student->getCompaniesBanned()->contains($company)){
            $data['success'] = false;
            $data['message'] = "L'étudiant ne souhaite pas prendre de rendez-vous avec vous. Veuillez contacter l'administateur si vous souhaitez un rendez-vous.";
            return new JsonResponse($data, 200);
        }

        $repoMeeting = $em->getRepository('MeetingBundle:Meeting');
        $meeting = $repoMeeting->findOneBy(['company' => $company, 'student' => $student]);
        if(! empty($meeting)){
            $data['success'] = false;
            $data['message'] = "Vous avez déjà un rendez-vous avec cette personne.";
            return new JsonResponse($data, 200);
        }

        $meeting = new Meeting();
        $meeting->setStudent($student);
        $meeting->setCompany($company);
        $meeting->setPriority($priority);

        $em->persist($meeting);
        $em->flush();

        $data['success'] = true;
        $data['message'] = "Le rendez-vous a été planifié.";

        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/meeting/delete/{meeting}", name="company_meeting_delete", options={"expose"=true})
     * @Security("has_role('ROLE_COMPANY')")
     * @param Request $request
     * @param Meeting $meeting
     * @return JsonResponse
     */
    public function deleteMeetingAction(Request $request, Meeting $meeting){

        if($meeting->getStartHour() != null){
            $data['success'] = false;
            $data['message'] = "Le planning a déjà été généré.";

            return new JsonResponse($data, 200);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($meeting);
        $em->flush();

        $data['success'] = true;
        $data['message'] = "Le rendez-vous a été supprimé.";
        return new JsonResponse($data, 200);
    }

    /**
    * @Route("/meeting/show/{meeting}", name="show_meeting")
    * @Security("has_role('ROLE_COMPANY')")
    * @param Request $request
    * @param Meeting $meeting
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function showMeetingAction(Request $request, Meeting $meeting){
        // TODO récupérer le prochain meeting en fonction de l'heure de fin, de l'id de l'entreprise et de l'id de l'étudiant
        $em = $this->getDoctrine()->getManager();

        $repoCommentary = $em->getRepository('UserBundle:Commentary');
        $commentary = $repoCommentary->findOneBy(['company' => $meeting->getCompany(), 'student' => $meeting->getStudent()]);

        $nextCompanyMeeting = null;
        $nextStudentMeeting = null;

        if($commentary == null)
            $commentary = new Commentary();

        $repoMeeting = $em->getRepository('MeetingBundle:Meeting');
        $commentaryType = $this->createForm(new CommentaryType(), $commentary);

        if($meeting->getStartHour() != null){
            $currentMeetingStartHour = clone $meeting->getStartHour();
            $currentMeetingDuration = $meeting->getDuration();
            $nextMeetingStartHour = date_add($currentMeetingStartHour, new \DateInterval('PT' . strval($currentMeetingDuration * 60) . 'S'));


            // Getting next company meeting
            $nextCompanyMeeting = $repoMeeting->findOneBy(array(
                'startHour' => $nextMeetingStartHour,
                'company' => $meeting->getCompany()
            ));
            // Getting next student meeting
            $nextStudentMeeting  = $repoMeeting->findOneBy(array(
                'startHour' => $nextMeetingStartHour,
                'student' => $meeting->getStudent()
            ));
        }

        $nextMeeting = $repoMeeting->findNextMeeting($meeting->getCompany(), $meeting->getStartHour());

        return $this->render('UserBundle:Company:show_meeting.html.twig', ['meeting' => $meeting, 'nextCompanyMeeting' => $nextCompanyMeeting, 'nextStudentMeeting' => $nextStudentMeeting, 'commentary' => $commentaryType->createView(), 'nextMeeting' => $nextMeeting]);
    }

    /**
     * @Route("/planning", name="company_planning")
     * @Security("has_role('ROLE_COMPANY')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function planningAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $repoCompany = $em->getRepository('UserBundle:Company');
        $company = $repoCompany->findOneBySupervisor($this->getUser());
        if(empty($company))
            throw new AccessDeniedException();

        $repoMeeting = $em->getRepository('MeetingBundle:Meeting');
        $meetings = $repoMeeting->findBy(
            ['company' => $company],
            ['startHour' => 'ASC']
        );

        return $this->render('UserBundle:Company:planning.html.twig', ['meetings' => $meetings]);
    }

    /**
     * @Route("/commentary/{meeting}/{content}", name="company_commentary_content", options={"expose"=true})
     * @Route("/commentary/{meeting}", name="company_commentary", options={"expose"=true})
     * @Security("has_role('ROLE_COMPANY')")
     * @param Request $request
     * @param Meeting $meeting
     * @param $content
     * @return JsonResponse
     */
    public function updateCommentaryAction(Request $request, Meeting $meeting, $content = null){

        if(!$request->isXmlHttpRequest())
            throw new AccessDeniedException();

        $em = $this->getDoctrine()->getManager();
        $repoCompany = $em->getRepository('UserBundle:Commentary');
        $commentary = $repoCompany->findOneBy(['company' => $meeting->getCompany(),'student' => $meeting->getStudent()]);
        if($commentary == null){
            $commentary = new Commentary();
            $commentary->setStudent($meeting->getStudent());
            $commentary->setCompany($meeting->getCompany());
        }
        $commentary->setContent($content);
        $em->persist($commentary);
        $em->flush();

        $data['success'] = true;
        return new JsonResponse($data, 200);
    }
}