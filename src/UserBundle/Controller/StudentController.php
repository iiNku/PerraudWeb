<?php
/**
 * Created by IntelliJ IDEA.
 * User: Pierre-Mac
 * Date: 02/09/15
 * Time: 16:17
 */

namespace UserBundle\Controller;


use FOS\UserBundle\Form\Type\RegistrationFormType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UserBundle\Entity\Company;
use UserBundle\Entity\Student;
use UserBundle\Entity\User;
use UserBundle\Form\ImageType;
use UserBundle\Form\StudentType;
use UserBundle\Form\StudentEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use UserBundle\Form\UserType;

class StudentController extends Controller{

    /**
     * @Route("/show/{id}", name="show_student")
     * @Security("has_role('ROLE_COMPANY') or has_role('ROLE_ADMIN')")
     * @Template()
     */
    function showAction(Student $student) {
        return ['student' => $student];
    }

    /**
     * @Route("/edit", name="edit_student")
     * @Security("has_role('ROLE_STUDENT')")
     * @Template()
     */
    function editAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $repoStudent = $em->getRepository('UserBundle:Student');
        $student = $repoStudent->findOneByUser($this->getUser());

        if(empty($student))
            throw new AccessDeniedException();

        $user = $this->getUser();

        $form = $this->createForm(new StudentEditType(), $student);
        $formImage = $this->createForm(new ImageType(), $user);

        $form->handleRequest($request);
        $formImage->handleRequest($request);

        if($form->isValid() && $formImage->isValid()){

            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updateUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $request->getSession()->getFlashBag()->add("message", "Votre profil a été mis à jour.");
            return $this->redirect($this->generateUrl('edit_student'));
        }

        return ['form' => $form->createView(), 'formImage' => $formImage->createView()];
    }

    /**
     * @param Request $request
     * @return array
     * @Route("/company", name="list_company_student")
     * @Security("has_role('ROLE_STUDENT')")
     * @Template
     */
    public function companyAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $companies = $em->getRepository('UserBundle:Company')->findAll();

        $repoStudent = $em->getRepository('UserBundle:Student');
        $student = $repoStudent->findOneByUser($this->getUser());

        $banned = $student->getCompaniesBanned();
        foreach($companies as $company){
            if($banned->contains($company))
                $company->setBanned(true);
        }


        return ['companies' => $companies];
    }

    /**
     * @Route("/search/{name}", name="search_student", options={"expose"=true})
     * @Security("has_role('ROLE_STUDENT')")
     * @param Request $request
     * @param $name
     * @return JsonResponse
     * @throws \Exception
     * @throws \Twig_Error
     */
    public function searchAction(Request $request, $name){

        if(!$request->isXmlHttpRequest())
            throw new AccessDeniedException();

        $em = $this->getDoctrine()->getManager();
        $repoStudent = $em->getRepository('UserBundle:Student');

        if($name == "#all")
            $students = $repoStudent->findAll();
        else
            $students = $repoStudent->searchByName($name);

        $engine = $this->container->get('templating');
        $template = $engine->render('UserBundle:Company:list_student.html.twig', ['students' => $students]);

        $data['success'] = true;
        $data['data'] = $template;

        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/company/ban", name="student_ban_company_list")
     * @Security("has_role('ROLE_STUDENT')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listBanAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $repoStudent = $em->getRepository('UserBundle:Student');

        $student = $repoStudent->findOneByUser($this->getUser());
        if(empty($student))
            throw new AccessDeniedException();

        return $this->render('UserBundle:Student:list_company_ban.html.twig', ['companies' => $student->getCompaniesBanned()]);
    }

    /**
     * @Route("/company/ban/{company}", name="student_ban_company", options={"expose"=true})
     * @Security("has_role('ROLE_STUDENT')")
     * @param Request $request
     * @param Company $company
     * @return JsonResponse
     */
    public function addBanAction(Request $request, Company $company){

        if(!$request->isXmlHttpRequest())
            throw new AccessDeniedException();

        $em = $this->getDoctrine()->getManager();
        $repoStudent = $em->getRepository('UserBundle:Student');

        $student = $repoStudent->findOneByUser($this->getUser());
        if(empty($student))
            throw new AccessDeniedException();

        if($student->getCompaniesBanned()->contains($company)){
            $data['success'] = true;
            $data['message'] = "Vous avez déjà interdit cette entreprise.";

            return new JsonResponse($data, 200);
        }

        $student->addBan($company);
        $em->persist($student);
        $em->flush();

        $data['success'] = true;
        $data['message'] = "L'entreprise ne peut désormais plus prendre de rendez-vous avec vous.";

        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/company/ban/remove/{company}", name="student_ban_company_remove", options={"expose"=true})
     * @Security("has_role('ROLE_STUDENT')")
     * @param Request $request
     * @param Company $company
     * @return JsonResponse
     */
    public function removeBanAction(Request $request, Company $company){
        if(!$request->isXmlHttpRequest())
            throw new AccessDeniedException();

        $em = $this->getDoctrine()->getManager();
        $repoStudent = $em->getRepository('UserBundle:Student');

        $student = $repoStudent->findOneByUser($this->getUser());
        if(empty($student))
            throw new AccessDeniedException();

        $student->removeBan($company);
        $em->persist($student);
        $em->flush();

        $data['success'] = true;
        $data['message'] = "L'entreprise peut à nouveau prendre rendez-vous avec vous.";

        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/meeting", name="student_meeting_list")
     * @Security("has_role('ROLE_STUDENT')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function meetingAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $repoStudent = $em->getRepository('UserBundle:Student');

        $student = $repoStudent->findOneByUser($this->getUser());
        if(empty($student))
            throw new AccessDeniedException();

        $repoMeeting = $em->getRepository('MeetingBundle:Meeting');
        $meetings = $repoMeeting->findBy(['student' => $student]);

        return $this->render('UserBundle:Student:meetings.html.twig', ['meetings' => $meetings]);
    }

    /**
     * @Route("/planning", name="student_planning")
     * @Security("has_role('ROLE_STUDENT')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function planningAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $repoStudent = $em->getRepository('UserBundle:Student');
        $student = $repoStudent->findOneByUser($this->getUser());
        if(empty($student))
            throw new AccessDeniedException();

        $repoMeeting = $em->getRepository('MeetingBundle:Meeting');
        $meetings = $repoMeeting->findBy(
            ['student' => $student],
            ['startHour' => 'ASC']
        );

        return $this->render('UserBundle:Student:planning.html.twig', ['meetings' => $meetings]);
    }
}