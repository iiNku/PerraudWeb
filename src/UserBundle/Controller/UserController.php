<?php
/**
 * Created by PhpStorm.
 * User: iinku
 * Date: 21/10/15
 * Time: 10:59
 */

namespace UserBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use UserBundle\Entity\Message;
use UserBundle\Form\AdminEditType;
use UserBundle\Form\MessageType;

class UserController extends Controller
{
    /**
     * @Route("/enable/{token}", name="enable_account")
     * @Template()
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function enableAction(Request $request, $token){

        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByConfirmationToken($token);
        if (null === $user) {
            throw new NotFoundHttpException("L'utilisateur n'existe pas.");
        }
        $form = $this->createForm(new AdminEditType(), $user);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $userManager = $this->container->get('fos_user.user_manager');
            $user->setConfirmationToken(null);
            $user->setEnabled(true);
            $userManager->updateUser($user);


            $request->getSession()->getFlashBag()->add("message", "Votre compte a été activé avec succès.");
            return $this->redirect($this->generateUrl('dashboard'));
        }
        return ['form' => $form->createView()];
    }

    /**
     * @Route("/message/send", name="message_post")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function postMessageAction(Request $request){

        $message = new Message();
        $form = $this->createForm(new MessageType(), $message);
        $form->handleRequest($request);
        if($form->isValid()){

            $message->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();

            $request->getSession()->getFlashBag()->add('message', 'Votre message a bien été envoyé.');
            return $this->redirect($this->generateUrl('dashboard'));
        }
        return $this->render('UserBundle:User:post_message.html.twig', ['form' => $form->createView()]);
    }

}