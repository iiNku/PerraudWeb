<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CompanyEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('url');
        $builder->add('description');
        $builder->add('name', 'hidden');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'userbundle_company_edit';
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return new CompanyType();
    }
}
