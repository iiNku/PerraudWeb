<?php
/**
 * Created by PhpStorm.
 * User: iinku
 * Date: 26/10/15
 * Time: 07:49
 */

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ImageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('imageFile', 'file', array('required' => false));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'userbundle_image';
    }
}