<?php
/**
 * Created by PhpStorm.
 * User: iinku
 * Date: 29/10/15
 * Time: 17:17
 */

namespace UserBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MessageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('message');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'userbundle_message_type';
    }
}