<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StudentEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('birthday', 'birthday', array(
            'required' => false,
            'years' => range(date('Y') - 100, date('Y'))
        ));
        $builder->add('sex','choice', array(
            'placeholder' => 'Votre sexe',
            'choices'  => array('M' => 'Homme', 'F' => 'Femme'),
            'required' => false)
        );
        $builder->add('description');
        $builder->add('nationality','choice', array(
            'placeholder' => 'Votre nationalité',
            'choices'  => array(
                'France' => 'France',
                'Americain' => 'Americain'
            ),
            'required' => false)
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'userbundle_student';
    }
}
