var socket = io('http://forum-polytech.univ-lyon1.fr:8080');


/*
 * Receive notification
 */
var nbUnreadNotifs = $.session.get('nbUnreadNotifs');
var nbUnreadNotifsTag = $('#nb-notifs-tag');
var notifsMenu = $('#notifs-menu');
var notifsList = $('#notifs');
var notifs = $.session.get('notifs');

if(notifs !== undefined && notifs !== '') {
	notifs = notifs.split(';');
	notifs.forEach(function(notif) {
		addNotifToList(notif);
	});
} else {
	notifs = new Array();
}

if(nbUnreadNotifs !== undefined && nbUnreadNotifs !== 0 && nbUnreadNotifs !== '0') {
	nbUnreadNotifsTag.text(nbUnreadNotifs);
	nbUnreadNotifsTag.show();
} else {
	nbUnreadNotifs = 0;
}

socket.on('late', function(to, msg) {
	var userId = $('#id').val();
	if(to == userId) {
		addNotifToList(msg);
		Messenger().post({
			message: msg
		});
		notifs.push(msg);
		$.session.set('notifs', notifs.join(';'));

		nbUnreadNotifs++;
		nbUnreadNotifsTag.text(nbUnreadNotifs);
		nbUnreadNotifsTag.show();
		$.session.set('nbUnreadNotifs', nbUnreadNotifs);
	}
});

function addNotifToList(text) {
	// Create notification div
	var div = $('<div/>');
	div.addClass('item');
	div.text(text);

	// Add icon
	var i = $('<i/>');
	i.addClass("angle right icon");
	div.prepend(i);

	// Add notification
	notifsList.prepend(div);
}

$(function() {
	$('#notifs-menu, #notifs-menu i').on('click', function(e) {
		nbUnreadNotifs = 0;
		nbUnreadNotifsTag.text('');
		nbUnreadNotifsTag.hide();
		$.session.set('nbUnreadNotifs', nbUnreadNotifs);
	});

	$('#btn-clear-notifs').on('click', function(e) {
		for(var i = notifsList.children().length - 1; i >= 0; i--) {
			if(notifsList.children()[i].getAttribute('id') != 'btn-clear-notifs') {
				notifsList.children()[i].remove();
			}
		}
		notifs.length = 0;
		$.session.clear();
	});
});


/*
 * Send notification
 */
$(function() {
	$('#btn-send-notif').on('click', function(e) {
		var currentCompanyName = $(this).data('company');
		var currentStudentName = $(this).data('student');

		var nextCompanyId = $(this).data('next-company-id');
		var nextStudentId = $(this).data('next-student-id');

		if(nextStudentId != '' && nextStudentId != '0' && nextStudentId != 0) {
			sendNotif(currentCompanyName, nextStudentId);
		}
		if(nextCompanyId != '' && nextCompanyId != '0' && nextCompanyId != 0) {
			sendNotif(currentStudentName, nextCompanyId);
		}
	});
});

function sendNotif(fromName, toId) {
	socket.emit('notification', fromName, toId);
}
